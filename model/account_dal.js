var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT first_name, last_name, email, school_name, skill_name, company_name FROM account a ' +
        'LEFT JOIN account_school ac on ac.account_id = a.account_id ' +
        'LEFT JOIN school s on s.school_id = ac.school_id ' +
        'LEFT JOIN account_skill acs on acs.account_id = a.account_id ' +
        'LEFT JOIN skill sk on sk.skill_id = acs.skill_id ' +
        'LEFT JOIN account_company acc on acc.account_id = a.account_id ' +
        'LEFT JOIN company c on c.company_id = acc.company_id '+
        'WHERE a.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query,queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO account (first_name,last_name,email) VALUES (?,?,?)';

    var queryData = [params.first_name, params.last_name, params.email];

    connection.query(query, queryData, function (err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_school (account_id, school_id) VALUES (?,?)';

        var queryData = [account_id, params.school_id];

        connection.query(query, queryData, function (err, result) {

            var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES (?,?)';

            var queryData = [account_id, params.skill_id];

            connection.query(query, queryData, function (err, result) {

                var query = 'INSERT INTO account_company (account_id, company_id) VALUES (?,?)';
                var queryData = [account_id, params.company_id];

                connection.query(query,queryData, function(err,result){

                    callback(err, result);

                })
            })
        })

    })



};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            companyAddressData.push([company_id, addressIdArray[i]]);
        }
    }
    else {
        companyAddressData.push([company_id, addressIdArray]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account SET first_name = ?, last_name = ?, email = ? WHERE account_id = ?';
    var queryData = [params.first_name,params.last_name,params.email, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        var query = 'UPDATE account_school SET school_id = ?, WHERE account_id = ?';
        var queryData = [params.school_id, params.account_id];

        connection.query(query, queryData, function(err, result) {
            var query = 'UPDATE account_skill SET skill_id = ?, WHERE account_id = ?';
            var queryData = [params.skill_id, params.account_id];

            connection.query(query, queryData, function(err, result) {
                var query = 'UPDATE account_company SET company_id = ?, WHERE account_id = ?';
                var queryData = [params.company_id, params.account_id];

                connection.query(query,queryData,function(err, result){
                    callback(err,result);

                })
            })
        })

    });



};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (_company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo5(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};